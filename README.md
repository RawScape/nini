# Nini
Nini is an uncommonly powerful .NET configuration library designed to help build highly configurable applications quickly.
Originally hosted at http://nini.sourceforge.net/ by bmatzelle.
Base on Version 1.1.

* Adapted for .NET Standard 2.1
* Adapted Unit Test to NUnit 3.0
* Changed Registry Unit Tests to Current User so that no admin privileges are required to run the tests
